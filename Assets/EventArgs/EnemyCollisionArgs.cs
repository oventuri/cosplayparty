﻿using Assets.Interfaces;

namespace Assets.EventArgs
{
    public class EnemyCollisionArgs :System.EventArgs
    {
        public IEnemy CurrentEnemy { get; internal set; }
    }
}
