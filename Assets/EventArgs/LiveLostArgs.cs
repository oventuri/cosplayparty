﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.EventArgs
{
    public class LiveLostArgs : System.EventArgs
    {
        public int RemainingLives { get; set; }
    }
}
