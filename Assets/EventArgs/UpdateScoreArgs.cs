﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.EventArgs
{
    public class UpdateScoreArgs : System.EventArgs
    {
        public int Score { get; set; }
    }
}
