﻿using System;
using System.Collections.Generic;
using System.Threading;
using Assets.EventArgs;
using Assets.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour, IDisposable
    {
        #region CONSTS
        private const int NUM_OF_BUTTONS = 3;
        private const int NUM_OF_LIVES = 3;
        private const int CHRONO_SECONDS = 1;
        private const string SCORE_KEY = "BESTSCORE";
        #endregion

        #region FIELDS
        private static GameManager _gameManager;
        private IEnemy _currentEnemy;
        private IHero _hero;
        private List<IEnemy> _enemies;
		private List<IHero> _heroes;
        private List<IButton> _buttons;
        private int _currentHeroIdx;
        private int _remainingLives;
        private int _score;
        private int _enemyKilled;
		private int _levelTimer; // velocità del timer per calcolo punteggio
        private Timer _timer;
        private TimeSpan _chrono;
        public static bool CanRestart = true;
        public float EnemyMultiplyer0_20 = 0.75f;
        public float EnemyMultiplyer21_40 = 0.9f;
        public float EnemyMultiplyer41_60 = 1.0f;
        public float EnemyMultiplyer61_80 = 1.25f;
        public float EnemyMultiplyer81_inf = 1.5f;
        #endregion

        #region EVENTS
        public event EventHandler AttackFail;
        public event EventHandler GameOver;
        public event EventHandler<LiveLostArgs> LiveLost;
        public event EventHandler<EnemyCollisionArgs> EnemyDead;
        public event EventHandler<UpdateScoreArgs> ScoreUpdated;
        #endregion
		

        public static GameManager Get
        {
            get
            {
                if (_gameManager == null) {
                    return (new GameObject("GameManager")).AddComponent<GameManager>();
                }
                return _gameManager;
            }
        }

        void Awake()
        {
            if (_gameManager == null) {
                _gameManager = this;
                DontDestroyOnLoad(gameObject);
            } else if (_gameManager != this) {
                Destroy(gameObject);
            }
        }

        void Start()
        {
			//NewGame();
        }

		void OnLevelWasLoaded(int level) {
            if (level == 1 && GameManager.CanRestart)
            {
                GameManager.CanRestart = false;
                Debug.Log("----->GameManager::OnLevelWasLoaded");
				NewGame();
			}
		}

        #region PUBLIC PROPERTIES

        public IEnemy CurrentEnemy
        {
            get { return _currentEnemy; }
        }

        public IList<IButton> Buttons
        {
            get { return _buttons; }
        }

        public IHero Hero
        {
            get { return _hero; }
            set { _hero = value; }
        }

        public int RemainingLives
        {
            get { return _remainingLives; }
            private set { _remainingLives = value; }
        }

        public int Score
        {
            get { return _score; }
        }

        public int BestScore
        {
            get
            {
                if (PlayerPrefs.HasKey(SCORE_KEY))
                {
                    return PlayerPrefs.GetInt(SCORE_KEY);
                }
                else
                {
                    return 0;
                }
            }
        }

        public float EnemySpeedMultiplier { get; private set; }
        #endregion

        #region PUBLIC METHODS
        #region Buttons
        public void PressButtonA()
        {
            Attack(_buttons[0].EnemyIdx);
        }

        public void PressButtonB()
        {
            Attack(_buttons[1].EnemyIdx);
        }

        public void PressButtonC()
        {
            Attack(_buttons[2].EnemyIdx);
        }

        public void Switch()
        {
            if (_currentHeroIdx == 1)
            {
                _currentHeroIdx = 0;                
            }
            else
            {
                _currentHeroIdx = 1;
            }

            Hero = _heroes[_currentHeroIdx];
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }

        public void Exit()
        {
            throw new NotImplementedException();
        }
        #endregion

        public IPanel LoadNextEnemy()
        {

			Debug.Log("####@@@@@*****GameManager::LoadNextEnemy");

            _currentEnemy = _enemies[Random.Range(0, _enemies.Count)];
            var buttonSolutionIdx = Random.Range(0, NUM_OF_BUTTONS);
            var buttonCodes = new List<int> { _currentEnemy.Idx };

            // prepara la lista di codici dei pulsanti e registra subito il codice vincente

            for (int i = 0; i < NUM_OF_BUTTONS; i++) {
                if (i == buttonSolutionIdx) {
                    _buttons[i].EnemyIdx = _currentEnemy.Idx;
                } else {
                    int newCode = 0;
                    do {
                        newCode = _enemies[Random.Range(0, _enemies.Count)].Idx;
                    } while (buttonCodes.Contains(newCode));
                    buttonCodes.Add(newCode);
                    _buttons[i].EnemyIdx = newCode;
                }
            }

            _currentEnemy.Walk();
            _currentEnemy.IsAlive = true;

            return new Panel()
            {
                Enemy = _currentEnemy,
                Buttons = _buttons
            };
        }

        public void Collide()
        {
			Debug.Log("GameManager::Collide");
            if (CurrentEnemy.IsAlive) {
				Debug.Log("GameManager::Collide::IsAlive");
                // Se al momento della collisione il Cosplay è ancora vivo, allora verifico se scalare una vita oppure andare al GameOver
                if (RemainingLives > 1) {
					Debug.Log("GameManager::Collide::IsAlive::RemainingLives=" + RemainingLives.ToString());
                    RemainingLives--;
                    if (LiveLost != null) {
                        LiveLost(this, new LiveLostArgs() { RemainingLives = RemainingLives });
                    }
                } else {
                    StopTimer();
                    Dispose();
                    SaveScore(Score);
                    UnsubscribeEvents();
					GameObject.Find("InfoCollector").GetComponent<AudioSource>().Stop();
                    Application.LoadLevel("GameOver");

                }
            }
        }

        public void NewGame()
        {
			_timer = null;
			_buttons = new List<IButton>(){
				new Button(),
				new Button(),
				new Button()
			};

            EnemySpeedMultiplier = EnemyMultiplyer0_20;
			_remainingLives = NUM_OF_LIVES;
            _score = 0;
            _enemyKilled = 0;
			_levelTimer = 1;
            StartTimer();
			InitializeEnemies();
			InitializeHeroes();
            var Panel = LoadNextEnemy();
            GameObject.Find("PannelloSkill").GetComponent<SkillPanel>().SetButton(Panel);
        }

        #endregion

        #region PRIVATE METHODS
        private void Attack(int enemyIdx)
        {

            if (enemyIdx == CurrentEnemy.Idx && Hero.IsMale == CurrentEnemy.IsMale) {
                CurrentEnemy.IsAlive = false;
                _enemyKilled++;
                if (EnemyDead != null) {
                    EnemyDead(this, new EnemyCollisionArgs() { CurrentEnemy = CurrentEnemy });
                }
            } else {
                if (AttackFail != null) {
                    AttackFail(this, new System.EventArgs());
                }
            }
        }

        private IEnemy GetEnemy(int idx, bool isMale)
        {
            var a = Resources.Load("Prefab/cosplay");
            var b = Instantiate(a);
            var c = ((GameObject)b).GetComponent<Enemy>();
            var enemy = (IEnemy)c;
            //var enemy = (IEnemy)((GameObject)Instantiate(Resources.Load("Prefab/cosplay"))).GetComponent<Enemy>();
            enemy.Idx = idx;
            enemy.IsAlive = true;
            enemy.IsMale = isMale;

            return enemy;
        }

        private void InitializeEnemies()
        {
            // Initialize Enemies
            _enemies = new List<IEnemy>()
            {
                GameObject.Find("wolverine").GetComponent<Enemy>(),
                GameObject.Find("batman").GetComponent<Enemy>(),
                GameObject.Find("lara").GetComponent<Enemy>(),
                GameObject.Find("Chun li 1.0_8").GetComponent<Enemy>()
            };
            foreach (IEnemy en in _enemies) {
                en.IsAlive = true;
            }
        }

        private void InitializeHeroes()
        {
            _heroes = new List<IHero>()
            {
                GameObject.Find("princess").GetComponent<Hero>(),
                GameObject.Find("player").GetComponent<Hero>()
            };
            _currentHeroIdx = 1;
            Hero = _heroes[_currentHeroIdx];
            Hero.IsMale = true;
        }

        private void StartTimer()
        {
            if (_timer == null) {
                _chrono = TimeSpan.FromMilliseconds(0);
				InvokeRepeating("TickCallback",1,CHRONO_SECONDS);
            }
        }
         
        private void StopTimer()
        {
            DisposeTimer();
        }

        private void TickCallback()
        {
			Debug.Log("TickCallback");
            _chrono = _chrono.Add(TimeSpan.FromSeconds(CHRONO_SECONDS));
            if (_chrono.TotalSeconds <= 10) {
                _score = (int)_chrono.TotalSeconds + 10 * _enemyKilled;
            } else if (_chrono.TotalSeconds <= 20) {
                _score = (int)_chrono.TotalSeconds + 15 * _enemyKilled;
            } else if (_chrono.TotalSeconds <= 30) {
                _score = (int)_chrono.TotalSeconds + 20 * _enemyKilled;
            } else if (_chrono.TotalSeconds <= 40) {
                _score = (int)_chrono.TotalSeconds + 25 * _enemyKilled;
            } else if (_chrono.TotalSeconds <= 50) {
                _score = (int)_chrono.TotalSeconds + 30 * _enemyKilled;
            } else {
                _score = (int)_chrono.TotalSeconds + 35 * _enemyKilled;
            }
            if (ScoreUpdated != null) {
                ScoreUpdated(this, new UpdateScoreArgs() { Score = _score });
            }

			if (_chrono.TotalSeconds > 20 && _levelTimer == 1)
			{
                EnemySpeedMultiplier = EnemyMultiplyer21_40;
				DisposeTimer();
				_levelTimer = 2;
				InvokeRepeating("TickCallback",0,CHRONO_SECONDS/2.0F);
			}else if(_chrono.TotalSeconds > 40 && _levelTimer == 2){
                EnemySpeedMultiplier = EnemyMultiplyer41_60;
				DisposeTimer();
				_levelTimer = 3;
				InvokeRepeating("TickCallback",0,CHRONO_SECONDS/4.0F);
			}else if(_chrono.TotalSeconds > 60 && _levelTimer == 3){
                EnemySpeedMultiplier = EnemyMultiplyer61_80;
				DisposeTimer();
				_levelTimer = 4;
				InvokeRepeating("TickCallback",0,CHRONO_SECONDS/8.0F);
			}else if(_chrono.TotalSeconds > 80 && _levelTimer == 4){
                EnemySpeedMultiplier = EnemyMultiplyer81_inf;
				DisposeTimer();
				_levelTimer = 5;
				InvokeRepeating("TickCallback",0,CHRONO_SECONDS/16.0F);
			}
        }

        public void Dispose()
        {
            DisposeTimer();
        }

        private void DisposeTimer()
        {
            if (_timer != null) {
                _timer.Dispose();
                _timer = null;
            }
            CancelInvoke("TickCallback");
        }

        private void SaveScore(int score)
        {
            if (BestScore<score)
            {
                PlayerPrefs.SetInt(SCORE_KEY, score);
            }
        }

        private void UnsubscribeEvents()
        {
            /*
        public event EventHandler AttackFail;
        public event EventHandler GameOver;
        public event EventHandler<LiveLostArgs> LiveLost;
        public event EventHandler<EnemyCollisionArgs> EnemyDead;
        public event EventHandler<UpdateScoreArgs> ScoreUpdated;             
             */

            AttackFail = null;
            GameOver = null;
            LiveLost = null;
            EnemyDead = null;
            ScoreUpdated = null;
        }
        #endregion


    }
}
