﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Interfaces;

namespace Assets.Scripts
{
    public class Panel : IPanel
    {
//        public static IPanel Fake()
//        {
//            return new Panel()
//            {
//                Enemy = GameManager.FakeEnemyZero,
//                Buttons = new List<IButton>()
//                {
//                    new Button() {EnemyIdx = 2},
//                    new Button() {EnemyIdx = 0},
//                    new Button() {EnemyIdx = 1}
//                }
//            };
//        }
        public List<IButton> Buttons { get; internal set; }
        public IEnemy Enemy { get; internal set; }
    }
}
