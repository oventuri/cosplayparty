﻿using UnityEngine;
using System.Collections;
using Assets.Interfaces;

namespace Assets.Scripts
{
    public class PlayerControllers : MonoBehaviour
    {
        //position of heroes
        private Vector3 PlayerPos = new Vector3(-5, -2, 0);

        private Vector3 PrincessPos = new Vector3(-7, -2, 0);

        private Vector3 tmpPos;

        public GameObject Player;
        public GameObject Princess;

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {

            //            if (Input.GetKeyDown(KeyCode.S))
            //            {
            //                Debug.Log("Premuto il tasto s");
            //                GameManager.Get.Switch();
            //                Switch();
            //            }
        }


        public void Switch()
        {
            tmpPos = Princess.transform.position;
            Princess.transform.position = new Vector3(Player.transform.position.x, Princess.transform.position.y, Princess.transform.position.z);
            Player.transform.position = new Vector3(tmpPos.x, Player.transform.position.y, Player.transform.position.z);
        }

    }
}
