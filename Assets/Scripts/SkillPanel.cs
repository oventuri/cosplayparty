using UnityEngine;
using System.Collections;
using Assets.Scripts;
using Assets.Interfaces;


public class SkillPanel : MonoBehaviour {
	public GameObject BottoneA;
	public GameObject BottoneB;
	public GameObject BottoneC;

	public Sprite[] miniaturebottoni;
	// Use this for initialization
	void Awake () {
	

		BottoneA.renderer.enabled = false;
		BottoneB.renderer.enabled = false;
		BottoneC.renderer.enabled = false;

		GameManager.Get.EnemyDead += OnEnemyDead;
		GameManager.Get.EnemyDead += OnAttackFail;

	}

	void OnEnemyDead(object sender, Assets.EventArgs.EnemyCollisionArgs e){
		Debug.Log ("Enemy Morto");
	}
	void OnAttackFail(object sender, System.EventArgs e){
		Debug.Log ("Enemy Fallito");
	}

	void OnDestroy(){

        //GameManager.Get.EnemyDead -= OnEnemyDead;
        //GameManager.Get.EnemyDead -= OnAttackFail;
	}

	void HandleAttackFail (object sender, System.EventArgs e)
	{
		
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown (KeyCode.A)) {
//			Debug.Log ("Premuto il tasto A");
//			BottoneA.renderer.enabled=!BottoneA.renderer.enabled;
//
//			
//		}
//		if (Input.GetKeyDown (KeyCode.B)) {
//			Debug.Log ("Premuto il tasto B");
//			BottoneB.renderer.enabled=!BottoneB.renderer.enabled;
//			
//
//		}
//		if (Input.GetKeyDown (KeyCode.C)) {
//			Debug.Log ("Premuto il tasto C");
//			BottoneC.renderer.enabled=!BottoneC.renderer.enabled;
//			
			
		}

	public void SetButton (IPanel panel)
	{
		IPanel Bottoni = panel;
		BottoneA.GetComponent<SpriteRenderer> ().sprite = miniaturebottoni[panel.Buttons[0].EnemyIdx];
		BottoneB.GetComponent<SpriteRenderer> ().sprite = miniaturebottoni[panel.Buttons[1].EnemyIdx];
		BottoneC.GetComponent<SpriteRenderer> ().sprite = miniaturebottoni[panel.Buttons[2].EnemyIdx];
		BottoneA.renderer.enabled=true;
		BottoneB.renderer.enabled=true;
		BottoneC.renderer.enabled = true;
		//ATTIVAZIONE dei click
		BottoneA.collider2D.enabled = true;
		BottoneB.collider2D.enabled = true;
		BottoneC.collider2D.enabled = true;

		ButtonSkill.Reset ();
	}

	public void DisableButton()
	{
		BottoneA.collider2D.enabled = false;
		BottoneB.collider2D.enabled = false;
		BottoneC.collider2D.enabled = false;
	}



}
