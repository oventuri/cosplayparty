﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GameOver : MonoBehaviour
    {

        void Start()
        {
            //var but = GameObject.Find("PlayAgainButton").GetComponent<UnityEngine.UI.Button>();
            var bestScoreTxt = GameObject.Find("BestScoreText").GetComponent<Text>();
            var currentScoreTxt = GameObject.Find("MatchScoreText").GetComponent<Text>();
            var gm = GameManager.Get;

            bestScoreTxt.text = gm.BestScore.ToString();
            currentScoreTxt.text = gm.Score.ToString();
        }

        private void OnMouseDown()
        {
            NewGame();
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                NewGame();
            }
        }

        private void NewGame()
        {
            GameManager.CanRestart = true;
			if (!GameObject.Find ("InfoCollector").GetComponent<AudioSource> ().isPlaying) {
				GameObject.Find("InfoCollector").GetComponent<AudioSource>().Play();
			}

            Application.LoadLevel("livello");
        }
    }
}
