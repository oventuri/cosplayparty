﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class LivePresenter : MonoBehaviour
    {
        private GameManager _gm;

        public GameObject Live1;
        public GameObject Live2;
        public GameObject Live3;

        private void Awake()
        {
            
        }

        private void Start()
        {
            _gm = GameManager.Get;
            _gm.LiveLost += _gm_LiveLost;

			Debug.Log("LivePresenter::Start");

            Live1 = GameObject.Find("life_heartA");
            Live2 = GameObject.Find("life_heartB");
            Live3 = GameObject.Find("life_heartC");
        }

        void _gm_LiveLost(object sender, EventArgs.LiveLostArgs e)
        {
			Debug.Log("LivePresenter::_gm_LiveLost");

            if (e.RemainingLives==2)
            {
                //Live3.SetActive(false);
				Live3.gameObject.SetActive(false);
            }
            else if (e.RemainingLives == 1)
            {
                Live2.SetActive(false);
            }
        }



        private void Update()
        {
            
        }

		void OnDestroy(){
			//_gm.LiveLost -= _gm_LiveLost;
		}
    }
}
