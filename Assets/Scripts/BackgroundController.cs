﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class BackgroundController : MonoBehaviour
    {

        public float speed = 0.3f;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            float offset = Time.time * speed;
            renderer.material.mainTextureOffset = offset * Vector2.right;
        }
    }
}
