﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using Assets.Interfaces;

public class ButtonSkill : MonoBehaviour {
	public static int[] state=new int[]{0,0,0};
	public GameObject Button;
	public GameObject Heroes;
	public static bool pressed=false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (!pressed) {
			if (Input.GetKeyDown (KeyCode.Space)) {
				Debug.Log ("Premuto il tasto switch");
				GameManager.Get.Switch ();
				Heroes.GetComponent<PlayerControllers> ().Switch ();
				
			}

						if (Input.GetKeyDown (KeyCode.A)) {
								Debug.Log ("Mouse premuto su A");
								GameManager.Get.PressButtonA ();
								state [0] = 2;
								state [1] = 3;
								state [2] = 3;
			
								GameObject.Find ("BottoneB").GetComponent<SpriteRenderer> ().color = Color.grey;
								GameObject.Find ("BottoneC").GetComponent<SpriteRenderer> ().color = Color.grey;
								GameObject.Find ("PannelloSkill").GetComponent<SkillPanel> ().DisableButton ();
								pressed =true;
						}
						if (Input.GetKeyDown (KeyCode.S)) {
								Debug.Log ("Mouse premuto su B");
								GameManager.Get.PressButtonB ();
								state [0] = 3;
								state [1] = 2;
								state [2] = 3;
								GameObject.Find ("BottoneA").GetComponent<SpriteRenderer> ().color = Color.grey;
								GameObject.Find ("BottoneC").GetComponent<SpriteRenderer> ().color = Color.grey;
								GameObject.Find ("PannelloSkill").GetComponent<SkillPanel> ().DisableButton ();
								pressed =true;
						}
						if (Input.GetKeyDown (KeyCode.D)) {
								Debug.Log ("Mouse premuto su C");
								GameManager.Get.PressButtonC ();
								state [0] = 3;
								state [1] = 3;
								state [2] = 2;
								GameObject.Find ("BottoneA").GetComponent<SpriteRenderer> ().color = Color.grey;
								GameObject.Find ("BottoneB").GetComponent<SpriteRenderer> ().color = Color.grey;
								GameObject.Find ("PannelloSkill").GetComponent<SkillPanel> ().DisableButton ();
								pressed =true;
						}
						
				}

	}

	void OnMouseDown()
	{			
				if (Button.name == "BottoneA") {
						Debug.Log ("Mouse premuto su A");
						GameManager.Get.PressButtonA ();
						state [0] = 2;
						state [1] = 3;
						state [2] = 3;

						GameObject.Find ("BottoneB").GetComponent<SpriteRenderer> ().color = Color.grey;
						GameObject.Find ("BottoneC").GetComponent<SpriteRenderer> ().color = Color.grey;
						GameObject.Find ("PannelloSkill").GetComponent<SkillPanel> ().DisableButton ();
				}
				if (Button.name == "BottoneB") {
						Debug.Log ("Mouse premuto su B");
						GameManager.Get.PressButtonB ();
						state [0] = 3;
						state [1] = 2;
						state [2] = 3;
						GameObject.Find ("BottoneA").GetComponent<SpriteRenderer> ().color = Color.grey;
						GameObject.Find ("BottoneC").GetComponent<SpriteRenderer> ().color = Color.grey;
						GameObject.Find ("PannelloSkill").GetComponent<SkillPanel> ().DisableButton ();
				}
				if (Button.name == "BottoneC") {
						Debug.Log ("Mouse premuto su C");
						GameManager.Get.PressButtonC ();
						state [0] = 3;
						state [1] = 3;
						state [2] = 2;
						GameObject.Find ("BottoneA").GetComponent<SpriteRenderer> ().color = Color.grey;
						GameObject.Find ("BottoneB").GetComponent<SpriteRenderer> ().color = Color.grey;
						GameObject.Find ("PannelloSkill").GetComponent<SkillPanel> ().DisableButton ();
				}
				if (Button.name == "BottoneSwitch") {
					Debug.Log("Premuto il tasto switch");
					GameManager.Get.Switch();
					Heroes.GetComponent<PlayerControllers>().Switch();
				
				}

	}
		
	public static void Reset()
	{
		state=new int[]{0,0,0};
		pressed = false;
		GameObject.Find ("BottoneA").GetComponent<SpriteRenderer>().color= Color.white;
		GameObject.Find ("BottoneB").GetComponent<SpriteRenderer>().color= Color.white;
		GameObject.Find ("BottoneC").GetComponent<SpriteRenderer>().color= Color.white;
	}
}
