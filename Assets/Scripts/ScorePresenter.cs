﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ScorePresenter : MonoBehaviour
    {
        private GameManager _gm;
		private int _score;

		public Text ScorePanel;

        private void Awake()
        {
        }

        private void Start()
        {
            _gm = GameManager.Get;
            _gm.ScoreUpdated += _gm_ScoreUpdated;
			Debug.Log("ScorePresenter::Start");
			ScorePanel = GameObject.Find("MyScoreText").GetComponent<Text>();
        }

        void _gm_ScoreUpdated(object sender, EventArgs.UpdateScoreArgs e)
        {
			Debug.Log("ScorePresenter::_gm_ScoreUpdated");
			ScorePanel.text = e.Score.ToString();
		}

        private void Update()
        {

        }

		void OnDestroy(){
			//_gm.ScoreUpdated -= _gm_ScoreUpdated;
		}
    }
}
