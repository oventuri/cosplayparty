﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Pause : MonoBehaviour {

        private bool pauseGame = false;
        private bool showGUI=false;

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void Update () {
            if (Input.GetKeyDown (KeyCode.Escape))
            {	

                pauseGame = !pauseGame;

                if (pauseGame == true) 
                {
                    pauseGame = true;
                    showGUI=true;
                    Debug.Log ("Gioco in pausa");
                    //sono in pausa
                    //mostro la GUI della pausa
                }
                if (pauseGame == false)
                {
                    pauseGame = false;
                    showGUI=false;
                    Debug.Log ("Gioco non in pausa");
                    //la pausa finisce e torna il gioco
                }

                if (showGUI==true)
                {
                    //disegna la gui di Pausa.
                }
            }
        }
    }
}
