﻿using UnityEngine;
using System.Collections;
using Assets.Interfaces;
using Assets.EventArgs;

namespace Assets.Scripts
{
    public class Enemy : MonoBehaviour, IEnemy
    {
        public float speed = 10;
        public bool isMale;
        public int idx;
        private static float initialX = 10;
        public SkillPanel PannelloSkill;

        // Use this for initialization
        void Start()
        {
            iTween.Init(gameObject);
            ResetPosition();
            SetMale();
            PannelloSkill = GameObject.Find("PannelloSkill").GetComponent<SkillPanel>();
        }

        private void SetMale()
        {
            //if (isMale)
            // GetComponent<SpriteRenderer>().color = Color.red;
        }

        private void ResetPosition()
        {
            transform.position = new Vector2(initialX, transform.position.y);
        }

        // Update is called once per frame
        void Update()
        {
            /*if (Input.GetKeyDown(KeyCode.Space)) {
                var Panel = GameManager.Get.LoadNextEnemy();
                PannelloSkill.SetButton(Panel);
                //Walk();
            }*/
        }
        private void onEndOfTheWalk()
        {
            GetComponent<Animator>().SetTrigger("walk");
            Debug.Log("------->Enemy::onEndOfTheWalk");
            ResetPosition();
            var Panel = GameManager.Get.LoadNextEnemy();
            PannelloSkill.SetButton(Panel);
        }

        public int Idx { get { return idx; } set { idx = value; } }
        public bool IsMale { get { return isMale; } set { isMale = value; SetMale(); } }
        public bool IsAlive { get; set; }
        public void Walk()
        {
            Debug.Log("WALK!!!!!");
            GameManager.Get.EnemyDead += onDead;
            var esm = GameManager.Get.EnemySpeedMultiplier;
            iTween.MoveTo(gameObject, iTween.Hash("x", -10, "speed", esm * speed, "easetype", iTween.EaseType.linear, "oncomplete", "onEndOfTheWalk"));
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            //Debug.Log("***Enemy::OnTriggerEnter2D->other.name=" + other.name);
            //Debug.Log("***Enemy::OnTriggerEnter2D->isMale=" + isMale.ToString());
            if ((other.name == "player") != isMale) {
                GameManager.Get.Collide();
                //Debug.Log("collision");
            }
        }

        private void onDead(object sender, EnemyCollisionArgs e)
        {
            GameManager.Get.EnemyDead -= onDead;
            GetComponent<Animator>().SetTrigger("dead");
        }

        void OnDestroy()
        {
            //GameManager.Get.EnemyDead -= onDead;
            iTween.Stop();
        }

    }
}
